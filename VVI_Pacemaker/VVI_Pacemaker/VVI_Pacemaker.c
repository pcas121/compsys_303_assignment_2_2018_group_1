/*****************************************************************************/
/*                 G E N E R A T E D       C    C O D E                      */
/*****************************************************************************/
/* KIELER - Kiel Integrated Environment for Layout Eclipse RichClient        */
/*                                                                           */
/* http://www.informatik.uni-kiel.de/rtsys/kieler/                           */
/* Copyright 2014 by                                                         */
/* + Kiel University                                                         */
/*   + Department of Computer Science                                        */
/*     + Real-Time and Embedded Systems Group                                */
/*                                                                           */
/* This code is provided under the terms of the Eclipse Public License (EPL).*/
/*****************************************************************************/
char VRPTO;
char VPase;
char VSense;
char LRITO;
char _VVI_Pacemaker_local_LRI_stop;
char _VVI_Pacemaker_local_VRP_ex;
char _VVI_Pacemaker_local_LRI_start;
char _VVI_Pacemaker_local_LRI_ex;
char _VVI_Pacemaker_local_VRP_start;
char _VVI_Pacemaker_local_VRP_stop;
char g0;
char g1;
char g2;
char g3;
char PRE_g3;
char g4;
char g4b;
char g5;
char PRE_g5;
char g6;
char g7;
char g8;
char g9;
char g10;
char PRE_g10;
char g11;
char g11b;
char g12;
char PRE_g12;
char g13;
char g13b;
char g14;
char g15;
char g16;
char g17;
char g18;
char PRE_g18;
char g19;
char g20;
char g21;
char PRE_g21;
char g22;
char g22b;
char g23;
char g24;
char g25;
char g26;
char g27;
char PRE_g27;
char g28;
char g29;
char g30;
char PRE_g30;
char g31;
char g31b;
char g32;
char PRE_g32;
char g33;
char g34;
char g35;
char PRE_g35;
char g36;
char g36b;
char g37;
char g38;
char g39;
char PRE_g39;
char g40;
char g41;
char g42;
char g43;
char PRE_g43;
char g44;
char g45;
char g46;
char g47;
char g48;
char PRE_g48;
char g49;
char g50;
char g51;
char _GO;
char _cg4;
char _cg6;
char _cg13;
char _cg11;
char _cg14;
char _cg22;
char _cg24;
char _cg19;
char _cg23;
char _cg28;
char _cg31;
char _cg37;
char _cg33;
char _cg38;
char g8_e1;
char g16_e2;
char g25_e3;
char g41_e4;
char g45_e5;
char g46_e1;
char g50_e2;
int _PRE_GO;
void reset(){
   _GO = 1;
   _PRE_GO = 0;
   PRE_g3 = 0;
   PRE_g5 = 0;
   PRE_g10 = 0;
   PRE_g12 = 0;
   PRE_g18 = 0;
   PRE_g21 = 0;
   PRE_g27 = 0;
   PRE_g30 = 0;
   PRE_g32 = 0;
   PRE_g35 = 0;
   PRE_g39 = 0;
   PRE_g43 = 0;
   PRE_g48 = 0;
   return;
}
void tick(){
   if(_PRE_GO == 1){
      _GO = 0;
   }
   {
      g0 = _GO;
      g1 = g0;
      g2 = g1;
      g42 = g1;
      g44 =(PRE_g43);
      g43 =(g42||g44);
      if(g43){
         _VVI_Pacemaker_local_LRI_start = 0;
         _VVI_Pacemaker_local_LRI_ex = 0;
         _VVI_Pacemaker_local_LRI_stop = 0;
         _VVI_Pacemaker_local_VRP_start = 0;
         _VVI_Pacemaker_local_VRP_stop = 0;
         _VVI_Pacemaker_local_VRP_ex = 0;
      }
      g6 =(PRE_g5);
      _cg6 = VRPTO;
      g7 =(g6&&_cg6);
      if(g7){
         _VVI_Pacemaker_local_VRP_ex =(_VVI_Pacemaker_local_VRP_ex||1);
      }
      g47 = g0;
      g49 =(PRE_g48);
      g48 =(g47||g49);
      if(g48){
         VPase = 0;
      }
      g19 =(PRE_g18);
      _cg19 = VSense;
      g24 =(g19&&(!(_cg19)));
      _cg24 = VPase;
      g20 =((g24&&_cg24)||(g19&&_cg19));
      if(g20){
         _VVI_Pacemaker_local_VRP_start =(_VVI_Pacemaker_local_VRP_start||1);
      }
      g4 =(PRE_g3);
      g4b = g4;
      _cg4 = _VVI_Pacemaker_local_VRP_start;
      g3 =(g2||g7||(g4b&&(!(_cg4))));
      g5 =((g4b&&_cg4)||(g6&&(!(_cg6))));
      g9 = g1;
      g33 =(PRE_g32);
      _cg33 = VSense;
      g34 =(g33&&_cg33);
      if(g34){
         _VVI_Pacemaker_local_LRI_stop =(_VVI_Pacemaker_local_LRI_stop||1);
      }
      g13 =(PRE_g12);
      g13b = g13;
      _cg13 = _VVI_Pacemaker_local_LRI_stop;
      g14 =(g13b&&(!(_cg13)));
      _cg14 = LRITO;
      g15 =(g14&&_cg14);
      if(g15){
         _VVI_Pacemaker_local_LRI_ex =(_VVI_Pacemaker_local_LRI_ex||1);
      }
      g28 =(PRE_g27);
      _cg28 = VSense;
      g29 =(g28&&_cg28);
      if(g29){
         _VVI_Pacemaker_local_LRI_start =(_VVI_Pacemaker_local_LRI_start||1);
      }
      g36 =(PRE_g35);
      g36b = g36;
      if(g36b){
         _VVI_Pacemaker_local_LRI_start =(_VVI_Pacemaker_local_LRI_start||1);
      }
      g11 =(PRE_g10);
      g11b = g11;
      _cg11 = _VVI_Pacemaker_local_LRI_start;
      g10 =((g13b&&_cg13)||g15||(g11b&&(!(_cg11)))||g9);
      g12 =((g11b&&_cg11)||(g14&&(!(_cg14))));
      g17 = g1;
      g22 =(PRE_g21);
      g22b = g22;
      _cg22 = _VVI_Pacemaker_local_VRP_ex;
      g18 =((g22b&&_cg22)||g17||(g24&&(!(_cg24))));
      g23 =(g22b&&(!(_cg22)));
      _cg23 = VSense;
      g21 =((g23&&_cg23)||g20||(g23&&_cg23));
      g26 = g1;
      g27 =((g28&&(!(_cg28)))||g26);
      g31 =(PRE_g30);
      g31b = g31;
      _cg31 = _VVI_Pacemaker_local_VRP_ex;
      g30 =(g29||g36b||(g31b&&(!(_cg31))));
      g37 =(g33&&(!(_cg33)));
      _cg37 = _VVI_Pacemaker_local_LRI_ex;
      g32 =((g31b&&_cg31)||(g37&&(!(_cg37))));
      g40 =(PRE_g39);
      g38 =((g37&&_cg37)||g40);
      _cg38 = VPase;
      g35 =(g34||(g38&&_cg38));
      g39 =(g38&&(!(_cg38)));
      g8_e1 =(!((g4||g6)));
      g16_e2 =(!((g11||g13)));
      g25_e3 =(!((g19||g22)));
      g41_e4 =(!((g28||g31||g33||g36||g40)));
      g45_e5 =(!(g44));
      g46_e1 =(!((g4||g11||g19||g28||g6||g13||g22||g31||g33||g44||g36||g40)));
      g50_e2 =(!(g49));
   }
   PRE_g3 = g3;
   PRE_g5 = g5;
   PRE_g10 = g10;
   PRE_g12 = g12;
   PRE_g18 = g18;
   PRE_g21 = g21;
   PRE_g27 = g27;
   PRE_g30 = g30;
   PRE_g32 = g32;
   PRE_g35 = g35;
   PRE_g39 = g39;
   PRE_g43 = g43;
   PRE_g48 = g48;
   _PRE_GO = _GO;
   return;
}
