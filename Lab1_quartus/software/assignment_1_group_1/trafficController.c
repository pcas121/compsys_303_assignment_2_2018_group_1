/* Traffic Light Controller
 *
 * --- Code is best viewed with the tab size of 4. ---
 */

#include <system.h>
#include <sys/alt_alarm.h>
#include <sys/alt_irq.h>
#include <altera_avalon_pio_regs.h>
#include <alt_types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// A template for COMPSYS 303 Assignment 1
//
// NOTE: You do not need to use this! Feel free
// to write your own code from scratch if you
// want, this is purely an example

// FUNCTION PROTOTYPES
// Timer ISRs
alt_u32 tlc_timer_isr(void* context);
alt_u32 camera_timer_isr(void* context);

//  Misc
// Others maybe added eg LEDs / UART
void lcd_set_mode(unsigned int mode);

// TLC state machine functions
void init_tlc(void);
void simple_tlc(int* state);
void pedestrian_tlc(int* state);
void configurable_tlc(int* state);
int config_tlc(int *tl_state);
void camera_tlc(int* state);

// Button Inputs / Interrupts
void buttons_driver(int* button);
void handle_mode_button(unsigned int* taskid);
void handle_vehicle_button(int* state, int vehicle_detected, int vehicle_time_out);
void init_buttons_pio(void);
void NSEW_ped_isr(void* context, alt_u32 id);

// Red light Camera
void clear_vehicle_detected(void);
void vehicle_checked(void);
int is_vehicle_detected(void);
int is_vehicle_left(void);

// Configuration Functions
int update_timeout(void);
void config_isr(void* context, alt_u32 id);
void buffer_timeout(unsigned int value);
void timeout_data_handler(void);

void init_mode_3_button_pio(void);
void take_snapshot(void);

// CONSTANTS
#define OPERATION_MODES 0x03	// number of operation modes (00 - 03 = 4 modes)
#define CAMERA_TIMEOUT	2000	// timeout period of red light camera (in ms)
#define TIMEOUT_NUM 6			// number of timeouts
#define TIME_LEN 8				// buffer length for time digits

#define ESC 27 					// ASCII code For escape is 27 erase screen
#define CLEAR_LCD_SCREEN "[2J"		// move mouse cursor to home position

// GREEN LEDS SETTING FOR MODE 1,2
#define LEDG_R_R IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00100100)
#define LEDG_G_R IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00100001)
#define LEDG_Y_R IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00100010)
#define LEDG_R_G IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00001100)
#define LEDG_R_Y IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00010100)
#define LEDG_G_R_P1 IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b01100001)
#define LEDG_R_G_P2 IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b10100001)

/* easy to check
#define LEDG_R_R IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00000001)
#define LEDG_G_R IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00000010)
#define LEDG_Y_R IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00000100)
#define LEDG_R_G IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00001000)
#define LEDG_R_Y IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b00010000)
#define LEDG_G_R_P1 IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b01000000)
#define LEDG_R_G_P2 IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b10000000)
*/


// USER DATA TYPES
// Timeout buffer structure
typedef struct  {
	int index;
	unsigned int timeout[TIMEOUT_NUM];
} TimeBuf;


// GLOBAL VARIABLES
static alt_alarm tlc_timer;		// alarm used for traffic light timing
static alt_alarm camera_timer;	// alarm used for camera timing

// GLOBAL LCD FILE VARIABLE
static FILE *lcd;


// NOTE:
// set contexts for ISRs to be volatile to avoid unwanted Compiler optimisation
static volatile int tlc_timer_event = 0; // have to use pointer for the trigger event so I used gobalTrigger
static volatile int camera_timer_event = 0;
static volatile int pedestrianNS = 0;
static volatile int pedestrianEW = 0;

static volatile int NShandled = 0;
static volatile int EWhandled = 0;

// GLOBAL TRIGGER FOR EVERY 500ms
static volatile int* globalTrigger = 0;
static volatile void* gloTri;
// GLOBAL POINTER FOR BUTTONS
static volatile int* buttonValue = 0b111;
static volatile void* buttonContext;

// 4 States of 'Detection':
// Car Absent
// Car Enters
// Car is Detected running a Red
// Car Leaves
static int vehicle_detected = 0;

static int vehicle_time_out = 0;


// Traffic light timeouts
static unsigned int timeout[TIMEOUT_NUM] = {500, 6000, 2000, 500, 6000, 2000};
static TimeBuf timeout_buf = { -1, {500, 6000, 2000, 500, 6000, 2000} };
// GLOBAL COUNTDOWN VARIABLE FOR TIMER INTERRUPT
static unsigned int timeoutcd[TIMEOUT_NUM] = {1,12,4,1,12,4};
static unsigned int countdown = 0;	//initialize to 0
#define TIME_PER_COUNT 500			// 500ms per count/trigger


// UART
FILE* fp;

// Traffic light LED values
//static unsigned char traffic_lights[TIMEOUT_NUM] = {0x90, 0x50, 0x30, 0x90, 0x88, 0x84};
// NS RGY | EW RGY
// NR,NG | NY,ER,EG,EY
static unsigned char traffic_lights[TIMEOUT_NUM] = {0x24, 0x14, 0x0C, 0x24, 0x22, 0x21};

enum traffic_states {RR0, GR, YR, RR1, RG, RY};

static unsigned int mode = 0;
// Process states: use -1 as initialization state
static int proc_state[OPERATION_MODES + 1] = {-1, -1, -1, -1};

// Initialize the traffic light controller
// for any / all modes
void init_tlc(void)
{
	alt_alarm_stop(&tlc_timer);
	switch (mode)
	{
		case 0:
			globalTrigger = 0;
			gloTri = (void*)globalTrigger;
			//gloTri = tlc_timer_event;
			alt_alarm_start(&tlc_timer,500,tlc_timer_isr,gloTri);
			break;
		case 1:
			globalTrigger = 0;
			gloTri = (void*)globalTrigger;
			//gloTri = tlc_timer_event;
			alt_alarm_start(&tlc_timer,500,tlc_timer_isr,gloTri);
			init_buttons_pio(); 	// initial button
			/*
			buttonContext = (void*)buttonValue;
			IOWR_ALTERA_AVALON_PIO_CAP(KEYS_BASE,0);	// CLEAR THE EDGE CAPTURE REGISTER
			IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEYS_BASE,0x7); // ENABLE INTERRUPT FOR BUTTONS
			*/
			alt_irq_register(KEYS_IRQ,buttonContext,NSEW_ped_isr); // start the button interrupt
			break;
		case 2:

			break;
		case 3:

			globalTrigger = 0;
			gloTri = (void*)globalTrigger;
			//gloTri = tlc_timer_event;
			alt_alarm_start(&tlc_timer,500,tlc_timer_isr,gloTri);
			
			init_buttons_pio(); 	// initial button
			/*
			buttonContext = (void*)buttonValue;
			IOWR_ALTERA_AVALON_PIO_CAP(KEYS_BASE,0);	// CLEAR THE EDGE CAPTURE REGISTER
			IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEYS_BASE,0x7); // ENABLE INTERRUPT FOR BUTTONS
			*/			
			
			break;
	}
}
	
	
/* DESCRIPTION: Writes the mode to the LCD screen
 * PARAMETER:   mode - the current mode
 * RETURNS:     none
 */
void lcd_set_mode(unsigned int mode)
{
	fprintf(lcd, "%c%s", ESC, CLEAR_LCD_SCREEN);
	fprintf(lcd, "MODE: %d\n", mode);
}

/* DESCRIPTION: Performs button-press detection and debouncing
 * PARAMETER:   button - referenced argument to indicate the state of the button
 * RETURNS:     none
 */
void buttons_driver(int* button)
{
	// Persistant state of 'buttons_driver'
	static int state = 0;
	
	*button = 0;	// no assumption is made on intial value of *button
	// Debounce state machine
		// call handle_mode_button()
}


/* DESCRIPTION: Updates the ID of the task to be executed and the 7-segment display
 * PARAMETER:   taskid - current task ID
 * RETURNS:     none
 */
void handle_mode_button(unsigned int* taskid)
{
	// Increment mode
	// Update Mode-display
}


/* DESCRIPTION: Simple traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
void simple_tlc(int* state)
{
	if (*state == -1) {
		// Process initialization state
		countdown = timeoutcd[0]; 	// set the countdown to the first time interval in the array
		init_tlc();					// initialize the timer
		//gloTri = (void*)globalTrigger;
		//alt_alarm_start(&tlc_timer,500,tlc_timer_isr,gloTri);
		//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100100); //RR
		LEDG_R_R;
		(*state)++;

		return;
	}
	
	// If the timeout has occured 
	/*
		// Increase state number (within bounds) 
		// Restart timer with new timeout value
	*/
	if (*state == 0) 	//RR
	{

				if(countdown <= 0)
				{
					//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100001); //GR
					LEDG_G_R;
					*state = 1;
					countdown = timeoutcd[1];
				}
				else if (*globalTrigger == 1)
				{
					countdown--;
					*globalTrigger = 0;

				}
			}
	if (*state == 1)	//GR
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100010); //YR
				LEDG_Y_R;
				*state = 2;
				countdown = timeoutcd[2];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 2)	//YR
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b001000); 
				LEDG_R_R;
				*state = 3;
				countdown = timeoutcd[3];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 3) 	//RR
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b010000);
				LEDG_R_G;
				*state = 4;
				countdown = timeoutcd[4];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 4)	//RG
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b001100);
				LEDG_R_Y;
				*state = 5;
				countdown = timeoutcd[5];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 5)	//RY
	{
		if(countdown <= 0)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100100);
			LEDG_R_R;
			*state = 0;
			countdown = timeoutcd[0];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else
	{
		//alt_alarm_stop(&tlc_timer);
	}

}


/* DESCRIPTION: Handles the traffic light timer interrupt
 * PARAMETER:   context - opaque reference to user data
 * RETURNS:     Number of 'ticks' until the next timer interrupt. A return value
 *              of zero stops the timer. 
 */
alt_u32 tlc_timer_isr(void* context)
{
	volatile int* trigger = (volatile int*)context;
	*trigger = 1;
	//printf("%d",countdown);
	return 500;
}

/* DESCRIPTION: Initialize the interrupts for all buttons
 * PARAMETER:   none
 * RETURNS:     none
 */
void init_buttons_pio(void)
{
	// Initialize NS/EW pedestrian button
	pedestrianNS = 0;
	pedestrianEW = 0;
	NShandled = 0;
	EWhandled = 0;
	// Reset the edge capture register
	buttonContext = (void*)buttonValue;
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE,0);	// CLEAR THE EDGE CAPTURE REGISTER
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEYS_BASE,0b011); // ENABLE INTERRUPT FOR BUTTONS 0 and 1
	//alt_irq_register(KEYS_IRQ,buttonContext,NSEW_ped_isr);
}

void init_mode_3_button_pio(void){
	
	vehicle_detected = 0;
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE, 0);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEYS_BASE, 0b100);
	
}

void take_snapshot(void){
	
	/*#define ESC 27
	#define CLEAR_LCD_STRING "[2J"
	fprintf(lcd, "%c%s", ESC, CLEAR_LCD_STRING);
	fprintf(lcd, "Snapshot taken");
	*/
	clear_vehicle_detected();
	printf("Snapshot taken");
	
}


void init_mode_switches(void){
	
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(SWITCHES_BASE, 0);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(SWITCHES_BASE, 0b11); //Sets switches 0 and 1 as interrupts to determine mode
	
}

/* DESCRIPTION: Pedestrian traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
void pedestrian_tlc(int* state)
{	


	if (*state == -1) {
		// Process initialization state
		init_tlc();
		(*state)++;
		return;
	}
	//printf("%d",*state);
	// Same as simple TLC
	// with additional states / signals for Pedestrian crossings
	if (*state == 0) 	//RR
	{
		if(countdown <= 0 && pedestrianNS != 1)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100001); //GR
			LEDG_G_R;
			*state = 1;
			countdown = timeoutcd[1];
		}
		else if(countdown <= 0 && pedestrianNS == 1)
		{
			LEDG_G_R_P1;
			*state = 6;
			countdown = timeoutcd[1];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else if (*state == 1)	//GR
	{
		if(countdown <= 0)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100010); //YR
			LEDG_Y_R;
			*state = 2;
			countdown = timeoutcd[2];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else if (*state == 2)	//YR
	{
		if(countdown <= 0)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b001000); //
			LEDG_R_R;
			*state = 3;
			countdown = timeoutcd[3];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else if (*state == 3) 	//RR
		{
		if(countdown <= 0 && pedestrianEW != 1)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b010000);
			LEDG_R_G;
			*state = 4;
			countdown = timeoutcd[4];
		}
		else if(countdown <= 0 && pedestrianEW == 1)
		{
			LEDG_R_G_P2;
			*state = 7;
			countdown = timeoutcd[4];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else if (*state == 4)	//RG
	{
		if(countdown <= 0)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b001100);
			LEDG_R_Y;
			*state = 5;
			countdown = timeoutcd[5];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else if (*state == 5)	//RY
	{
		if(countdown <= 0)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100100);
			LEDG_R_R;
			*state = 0;
			countdown = timeoutcd[0];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else if (*state == 6)	//GRP1
	{
		if(countdown <= 0)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100100);
			LEDG_Y_R;
			*state = 2;
			countdown = timeoutcd[2];
			//IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE,0b10);	//clear the edge capture register for bt0
			pedestrianNS = 0;
			printf("pdns = 0\n");
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else if (*state == 7)	//RGP2
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100100);
				LEDG_R_Y;
				*state = 5;
				countdown = timeoutcd[5];
				pedestrianEW = 0;
				printf("pdew = 0\n");
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else
	{
		//alt_alarm_stop(&tlc_timer);
	}



}


/* DESCRIPTION: Handles the NSEW pedestrian button interrupt
 * PARAMETER:   context - opaque reference to user data
 *              id - hardware interrupt number for the device
 * RETURNS:     none
 */
void NSEW_ped_isr(void* context, alt_u32 id)
{
	// NOTE:
	// Cast context to volatile to avoid unwanted compiler optimization.
	// Store the value in the Button's edge capture register in *context
	volatile int* temp = (volatile int*)context;
	(*temp) = IORD_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE);
	if((*temp) == 0b01 || (*temp) == 0b11)  	// active high????
	{
		pedestrianNS = 1;
		printf("pdns = 1\n");
	}
	if((*temp) == 0b10 || (*temp) == 0b11)
	{
		pedestrianEW = 1;
		printf("pdew = 1\n");
	}
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEYS_BASE,0);
	printf("%d state: %d\n",*temp, proc_state[1]);
}


/* DESCRIPTION: Configurable traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
/*
If there is new configuration data... Load it.
Else run pedestrian_tlc();
*/
void configurable_tlc(int* state)
{	
	if (*state == -1) {
		// Process initialization state
		return;
	}
	
	
}


/* DESCRIPTION: Implements the state machine of the traffic light controller in 
 *              the ***configuration*** phase
 * PARAMETER:   tl_state - state of the traffic light
 * RETURNS:     Returns the state of the configuration phase
 */
/*
Puts the TLC in a 'safe' state... then begins update
*/
int config_tlc(int* tl_state)
{
	// State of configuration
	static int state = 0;
	
	if (*tl_state == -1) {
		// Process initialization state
		state = 0;
		return 0;
	}
	
	return state;
}


/* DESCRIPTION: Parses the configuration string and updates the timeouts
 * PARAMETER:   none
 * RETURNS:     none
 */
/*
 buffer_timeout() must be used 'for atomic transfer to the main timeout buffer'
*/
void timeout_data_handler(void)
{
	
}


/* DESCRIPTION: Stores the new timeout values in a secondary buffer for atomic 
 *              transfer to the main timeout buffer at a later stage
 * PARAMETER:   value - value to store in the buffer
 * RETURNS:     none
 */
void buffer_timeout(unsigned int value)
{
	
}


/* DESCRIPTION: Implements the update operation of timeout values as a critical 
 *              section by ensuring that timeouts are fully received before 
 *              allowing the update
 * PARAMETER:   none
 * RETURNS:     1 if update is completed; 0 otherwise
 */
int update_timeout(void)
{
	
}

/* DESCRIPTION: Handles the red light camera timer interrupt
 * PARAMETER:   context - opaque reference to user data
 * RETURNS:     Number of 'ticks' until the next timer interrupt. A return value
 *              of zero stops the timer. 
 */
alt_u32 camera_timer_isr(void* context)
{
	volatile int* trigger = (volatile int*)context;
	*trigger = 1;
	return 2000;
}	

/* DESCRIPTION: Camera traffic light controller
 * PARAMETER:   state - state of the controller
 * RETURNS:     none
 */
 /*
 Same functionality as configurable_tlc
 But also handles Red-light camera
 */
void camera_tlc(int* state)
{

	if (*state == -1) {
		// Process initialization state
		countdown = timeoutcd[0]; 	// set the countdown to the first time interval in the array
		init_tlc();					// initialize the timer
		//gloTri = (void*)globalTrigger;
		//alt_alarm_start(&tlc_timer,500,tlc_timer_isr,gloTri);
		//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100100); //RR
		LEDG_R_R;
		(*state)++;

		return;
	}
	
	// If the timeout has occured 
	/*
		// Increase state number (within bounds) 
		// Restart timer with new timeout value
	*/
	if (*state == 0) 	//RR
	{				
				if(countdown <= 0)
				{
					//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100001); //GR
					LEDG_G_R;
					*state = 1;
					countdown = timeoutcd[1];
				}
				else if (*globalTrigger == 1)
				{
					countdown--;
					*globalTrigger = 0;

				}
	}
	if (*state == 1)	//GR
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100010); //YR
				LEDG_Y_R;
				*state = 2;
				countdown = timeoutcd[2];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 2)	//YR
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b001000); //
				LEDG_R_R;
				*state = 3;
				countdown = timeoutcd[3];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 3) 	//RR
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b010000);
				LEDG_R_G;
				*state = 4;
				countdown = timeoutcd[4];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 4)	//RG
		{
			if(countdown <= 0)
			{
				//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b001100);
				LEDG_R_Y;
				*state = 5;
				countdown = timeoutcd[5];
			}
			else if (*globalTrigger == 1)
			{
				countdown--;
				*globalTrigger = 0;
			}
		}
	else if (*state == 5)	//RY
	{
		if(countdown <= 0)
		{
			//IOWR_ALTERA_AVALON_PIO_DATA(LEDS_GREEN_BASE,0b100100);
			LEDG_R_R;
			*state = 0;
			countdown = timeoutcd[0];
		}
		else if (*globalTrigger == 1)
		{
			countdown--;
			*globalTrigger = 0;
		}
	}
	else
	{
		//alt_alarm_stop(&tlc_timer);
	}
	
}


/* DESCRIPTION: Simulates the entry and exit of vehicles at the intersection
 * PARAMETER:   none
 * RETURNS:     none
 */
void handle_vehicle_button(int* state, int vehicle_detected, int vehicle_time_out)
{
	
	unsigned int button_input_value = IORD_ALTERA_AVALON_PIO_DATA(KEYS_BASE);
	
	if (mode == 3){
		if ((int)state == 0 || (int)state == 3){ //If state is RR
			if (button_input_value >= 0b100 || vehicle_time_out == 1){
				vehicle_detected = 2; //Vehicle has been detected running a red light
			}
		} else if (button_input_value >= 0b100 && (vehicle_detected == 0 || vehicle_detected == 3)){
			
			vehicle_detected = 1; //Vehicle has been detected but the lights aren't red
			
			printf("Camera activated");
			
			alt_alarm_start(&camera_timer, 0, camera_timer_isr, gloTri);
	
		} else if (button_input_value >= 0b100 && (vehicle_detected == 1 || vehicle_detected == 2)){
			
			vehicle_detected = 3; //Vehicle has been detected leaving the area
			
			printf("Vehicle left.\n");
			//int* time_left = camera_timer;
			//printf("Vehicle left after %d cycles", time_left);
			
			alt_alarm_stop(&camera_timer);
			
		}
		
	}
}

// set vehicle_detected to 'no vehicle' state
void clear_vehicle_detected(void) 
{  
	vehicle_detected = 0;
}
// set vehicle_detected to 'checking' state
void vehicle_checked(void) 
{
	vehicle_detected = 1;
}
// return true or false if a vehicle has been detected
int is_vehicle_detected(void) 
{
	if (vehicle_detected == 1) return 1;
	return 0;	
}
// return true or false if the vehicle has left the intersection yet
int is_vehicle_left(void) 
{	
	if (vehicle_detected == 3) return 1;
	return 0;
}





int main(void)
{	
	int buttons = 0;			// status of mode button
	lcd = fopen(LCD_NAME,"w"); 	// open LCD as a file
	
	lcd_set_mode(0);		// initialize lcd
	init_buttons_pio();			// initialize buttons
	init_mode_switches();	//initiates switches

	// set up global countdown number
	// timeout / 500 = time to trigger
	for (int i = 0;i < TIMEOUT_NUM;i++)
	{
		timeoutcd[i] = timeout[i]/TIME_PER_COUNT;
	}
	mode = 1;
	while (1) {
		// Button detection & debouncing
		
		// if Mode button pushed:
			// Set current TLC state to -1
			// handle_mode_button to change state & display
		// if Car button pushed...
			// handle_vehicle_button
    	
		// Execute the correct TLC
    	switch (mode) {
			case 0:
				simple_tlc(&proc_state[0]);
				break;
			case 1:
				pedestrian_tlc(&proc_state[1]);
				break;
			case 2:
				configurable_tlc(&proc_state[2]);
				break;
			case 3:
				handle_vehicle_button(&proc_state[3], vehicle_detected, vehicle_time_out);
				if (vehicle_detected == 2){
					take_snapshot();
				}
				camera_tlc(&proc_state[3]);
				if (vehicle_detected == 0){
					init_mode_3_button_pio();
				}
				break;
		}
		// Update Displays
	}
	return 1;
}
